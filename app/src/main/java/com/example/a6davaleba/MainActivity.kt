package com.example.a6davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.example.a6davaleba.Adapter.ViewPagerFragmentAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var  viewPager2: ViewPager2
    private lateinit var viewPager3: ViewPager3
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPagerFragmentAdapter: ViewPagerFragmentAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        viewPager2.adapter = viewPagerFragmentAdapter
        viewPager3.adapter = viewPagerFragmentAdapter
        TabLayoutMediator(tabLayout,viewPager2,viewPager3){tab,position->
            tab.text = "tab ${position+2}"

        }. attach()
    }
    private fun init(){
        viewPager2 = findViewById(R.id.viewPager2)
        viewPager3 = findViewById(R.id.viewPager3)
        tabLayout = findViewById(R.id.tablayout)
        viewPagerFragmentAdapter = ViewPagerFragmentAdapter(this)
    }

}